# translation of libgphoto2_port.po to Basque
# Basque Translation.
# Copyright � 2005 I�aki Larra�aga.
# I�aki Larra�aga <dooteo@euskalgnu.org>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: eu\n"
"Report-Msgid-Bugs-To: gphoto-devel@lists.sourceforge.net\n"
"POT-Creation-Date: 2022-12-20 16:18+0100\n"
"PO-Revision-Date: 2005-05-14 13:32+0200\n"
"Last-Translator: I�aki Larra�aga  <dooteo@euskalgnu.org>\n"
"Language-Team:  <itzulpena@euskalgnu.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"X-Generator: KBabel 1.9.1\n"

#: libgphoto2_port/gphoto2-port-info-list.c:449
msgid "Generic Port"
msgstr "Ataka generikoa"

#: libgphoto2_port/gphoto2-port-result.c:42
msgid "No error"
msgstr "Errorerik gabe"

#: libgphoto2_port/gphoto2-port-result.c:44
msgid "Unspecified error"
msgstr "Zehaztu gabeko errorea"

#: libgphoto2_port/gphoto2-port-result.c:46
msgid "I/O problem"
msgstr "S/I-ko arazoa"

#: libgphoto2_port/gphoto2-port-result.c:48
msgid "Bad parameters"
msgstr "Parametro okerrak"

#: libgphoto2_port/gphoto2-port-result.c:50
msgid "Unsupported operation"
msgstr "Eragiketa ez dago onartuta"

#: libgphoto2_port/gphoto2-port-result.c:52
msgid "Fixed limit exceeded"
msgstr ""

#: libgphoto2_port/gphoto2-port-result.c:54
msgid "Timeout reading from or writing to the port"
msgstr "Denbora-muga irungitu da atakan irakurtzean/idaztean"

#: libgphoto2_port/gphoto2-port-result.c:56
msgid "Serial port not supported"
msgstr "Serieko ataka ez dago onartuta"

#: libgphoto2_port/gphoto2-port-result.c:58
msgid "USB port not supported"
msgstr "USB ataka ez dago onartuta"

#: libgphoto2_port/gphoto2-port-result.c:60
msgid "Unknown port"
msgstr "Ataka ezezaguna"

#: libgphoto2_port/gphoto2-port-result.c:62
msgid "Out of memory"
msgstr "Memoriarik gabe"

#: libgphoto2_port/gphoto2-port-result.c:64
msgid "Error loading a library"
msgstr "Errorea liburutegia kargatzean"

#: libgphoto2_port/gphoto2-port-result.c:66
msgid "Error initializing the port"
msgstr "Errorea ataka hasieratzean"

#: libgphoto2_port/gphoto2-port-result.c:68
msgid "Error reading from the port"
msgstr "Errorea atakatik irakurtzean"

#: libgphoto2_port/gphoto2-port-result.c:70
msgid "Error writing to the port"
msgstr "Errorea atakan idaztean"

#: libgphoto2_port/gphoto2-port-result.c:72
msgid "Error updating the port settings"
msgstr "Errorea atakako konfigurazioa eguneratzean"

#: libgphoto2_port/gphoto2-port-result.c:74
msgid "Error setting the serial port speed"
msgstr "Errorea serieko atakaren abiadura ezartzean"

#: libgphoto2_port/gphoto2-port-result.c:76
msgid "Error clearing a halt condition on the USB port"
msgstr "Errorea USB atakako itzaltze-baldintza ezabatzean"

#: libgphoto2_port/gphoto2-port-result.c:78
msgid "Could not find the requested device on the USB port"
msgstr "Ezin izan da eskatutako gailua USB atakan aurkitu"

#: libgphoto2_port/gphoto2-port-result.c:80
msgid "Could not claim the USB device"
msgstr "Ezin izan da USB gailua aldarrikatu"

#: libgphoto2_port/gphoto2-port-result.c:82
msgid "Could not lock the device"
msgstr "Ezin izan da gailua blokeatu"

#: libgphoto2_port/gphoto2-port-result.c:84
msgid "libhal error"
msgstr ""

#: libgphoto2_port/gphoto2-port-result.c:86
msgid "Unknown error"
msgstr "Errore ezezaguna"

#: libgphoto2_port/gphoto2-port.c:47
#, c-format
msgid "The operation '%s' is not supported by this device"
msgstr "Gailu honek ez du '%s' eragiketa onartzen"

#: libgphoto2_port/gphoto2-port.c:48
msgid "The port has not yet been initialized"
msgstr "Ez da ataka hasieratu oraindik"

#: libgphoto2_port/gphoto2-port.c:686
msgid "low"
msgstr ""

#: libgphoto2_port/gphoto2-port.c:687
msgid "high"
msgstr ""

#: libgphoto2_port/gphoto2-port.c:1215
msgid "No error description available"
msgstr "Errorearen azalpena ez dago eskuragarri"

#: disk/disk.c:133 disk/disk.c:134 disk/disk.c:212 disk/disk.c:213
#: disk/disk.c:253 disk/disk.c:276
#, c-format
msgid "Media '%s'"
msgstr ""

#: libusb1/libusb1.c:386 usb/libusb.c:248
#, fuzzy, c-format
msgid "Could not open USB device (%s)."
msgstr "Ezin izan da USB gailua ireki (%m)."

#: libusb1/libusb1.c:395 usb/libusb.c:261
msgid "Camera is already in use."
msgstr "Kamera jadanik erabiltzen ari da"

#: libusb1/libusb1.c:404
#, fuzzy
msgid "Could not detach kernel driver of camera device."
msgstr "Ezin izan da kamera gailuaren kerneleko '%s' kontrolatzailea askatu"

#: libusb1/libusb1.c:410 usb/libusb.c:274
msgid "Could not query kernel driver of device."
msgstr "Ezin izan da gailuaren kontrolatzailea kernelari eskatu"

#: libusb1/libusb1.c:417 usb/libusb.c:283
#, fuzzy, c-format
msgid ""
"Could not claim interface %d (%s). Make sure no other program (%s) or kernel "
"module (such as %s) is using the device and you have read/write access to "
"the device."
msgstr ""
"Ezin izan da %d interfazea aldarrikatu (%m). Ziurta zaitez beste programa "
"batek edo kerneleko modulo batek (%s bezalakoak) gailua erabiltzen ari ez "
"dela, eta gailuan irakurtzeko/idazteko baimenak dituzula."

#: libusb1/libusb1.c:428 usb/libusb.c:295
msgid "MacOS PTPCamera service"
msgstr ""

#: libusb1/libusb1.c:430 usb/libusb.c:297
msgid "unknown libgphoto2 using program"
msgstr ""

#: libusb1/libusb1.c:499 usb/libusb.c:314
#, fuzzy, c-format
msgid "Could not release interface %d (%s)."
msgstr "Ezin izan da %d interfazea askatu (%m)."

#: libusb1/libusb1.c:513 usb/libusb.c:329
#, fuzzy, c-format
msgid "Could not reset USB port (%s)."
msgstr "Ezin izan da USB ataka itxi (%m)."

#: libusb1/libusb1.c:522
#, fuzzy
msgid "Could not reattach kernel driver of camera device."
msgstr "Ezin izan da kamera gailuaren kerneleko '%s' kontrolatzailea askatu"

#: libusb1/libusb1.c:956 usb/libusb.c:621
#, fuzzy, c-format
msgid "Could not set config %d/%d (%s)"
msgstr "Ezin izan da %d/%d konfigurazioa ezarri (%m)."

#: libusb1/libusb1.c:985 usb/libusb.c:652
#, fuzzy, c-format
msgid "Could not set altsetting from %d to %d (%s)"
msgstr "Ezin izan da %d/%d aukerazko konfigurazioa ezarri (%m)"

#: libusb1/libusb1.c:1119 libusb1/libusb1.c:1211 usb/libusb.c:812
#: usb/libusb.c:898
#, c-format
msgid ""
"Could not find USB device (vendor 0x%x, product 0x%x). Make sure this device "
"is connected to the computer."
msgstr ""
"Ezin izan da USB gailua (0x%x hornitzailea, 0x%x produktua) aurkitu. Ziurta "
"zaitez gailua ordenagailuari konektatuta dagoela."

#: libusb1/libusb1.c:1150 usb/libusb.c:741
#, c-format
msgid "The supplied vendor or product id (0x%x,0x%x) is not valid."
msgstr ""
"Zehaztutako hornitzailearen edo produktuaren IDa (0x%x,0x%x) ez da baliozkoa."

#: libusb1/libusb1.c:1500 usb/libusb.c:1191
#, c-format
msgid ""
"Could not find USB device (class 0x%x, subclass 0x%x, protocol 0x%x). Make "
"sure this device is connected to the computer."
msgstr ""
"Ezin izan da USB gailua aurkitu (0x%x klasea, 0x%x azpiklasea, 0x%x "
"protokoloa). Ziurta zaitez gailua ordenagailura konektatuta dagoela."

#: ptpip/ptpip.c:161
msgid "PTP/IP Connection"
msgstr ""

#: ptpip/ptpip.c:174
msgid "IP Connection"
msgstr ""

#: serial/unix.c:209
#, c-format
msgid "Could not lock device '%s'"
msgstr "Ezin izan da '%s' gailua blokeatu"

#: serial/unix.c:219 usbdiskdirect/linux.c:79
#, c-format
msgid "Device '%s' is locked by pid %d"
msgstr "'%s' gailua %d pid-ek blokeatuta dauka"

#: serial/unix.c:222 usbdiskdirect/linux.c:82
#, c-format
msgid "Device '%s' could not be locked (dev_lock returned %d)"
msgstr "Ezin izan da '%s' gailua blokeatu (dev_lock-ek %d itzuli du)"

#: serial/unix.c:251
#, c-format
msgid "Device '%s' could not be unlocked."
msgstr "Ezin izan da '%s' gailua desblokeatu."

#: serial/unix.c:263 usbdiskdirect/linux.c:111
#, c-format
msgid "Device '%s' could not be unlocked as it is locked by pid %d."
msgstr "Ezin izan da '%s' gailua desblokeatu, %d pid-ak blokeatuta baitauka."

#: serial/unix.c:267 usbdiskdirect/linux.c:115
#, c-format
msgid "Device '%s' could not be unlocked (dev_unlock returned %d)"
msgstr "Ezin izan da '%s' desblokeatu (dev_unlock-ek %d itzuli du)"

#: serial/unix.c:313
#, c-format
msgid "Serial Port %i"
msgstr "Serieko ataka: %i"

#: serial/unix.c:326
#, fuzzy
msgid "Serial Port Device"
msgstr "Serieko ataka: %i"

#: serial/unix.c:401
#, fuzzy, c-format
msgid "Failed to open '%s' (%s)."
msgstr "Huts egin du '%s' irekitzean (%m)."

#: serial/unix.c:421
#, fuzzy, c-format
msgid "Could not close '%s' (%s)."
msgstr "Ezin izan da '%s' itxi (%m)."

#: serial/unix.c:477
#, fuzzy, c-format
msgid "Could not write to port (%s)"
msgstr "Ezin izan da atakan idatzi (%m)"

#: serial/unix.c:552
msgid "Parity error."
msgstr "Paritate-errorea."

#: serial/unix.c:556
#, c-format
msgid "Unexpected parity response sequence 0xff 0x%02x."
msgstr "Ustegabeko 0xff 0x%02x paritate-erantzuna sekuentzia."

#: serial/unix.c:599
#, c-format
msgid "Unknown pin %i."
msgstr "%i PIN ezezaguna."

#: serial/unix.c:621
#, fuzzy, c-format
msgid "Could not get level of pin %i (%s)."
msgstr "Ezin izan da %i pin-aren maila lortu (%m)."

#: serial/unix.c:657
#, fuzzy, c-format
msgid "Could not set level of pin %i to %i (%s)."
msgstr "Ezin izan da %i pin-aren maila %i gisa ezarri (%m)."

#: serial/unix.c:684
#, fuzzy, c-format
msgid "Could not flush '%s' (%s)."
msgstr "Ezin izan da '%s' hustu (%m)."

#: serial/unix.c:778
#, c-format
msgid "Could not set the baudrate to %d"
msgstr "Ezin izan da %d(r)en baudio-tasa ezarri"

#: usb/libusb.c:269
#, c-format
msgid "Could not detach kernel driver '%s' of camera device."
msgstr "Ezin izan da kamera gailuaren kerneleko '%s' kontrolatzailea askatu"

#: usb/libusb.c:359
#, fuzzy, c-format
msgid "Could not close USB port (%s)."
msgstr "Ezin izan da USB ataka itxi (%m)."

#: usbdiskdirect/linux.c:218
msgid "USB Mass Storage direct IO"
msgstr ""

#: usbdiskdirect/linux.c:269 usbscsi/linux.c:253
#, c-format
msgid "Failed to open '%s' (%m)."
msgstr "Huts egin du '%s' irekitzean (%m)."

#: usbdiskdirect/linux.c:283 usbscsi/linux.c:281
#, c-format
msgid "Could not close '%s' (%m)."
msgstr "Ezin izan da '%s' itxi (%m)."

#: usbdiskdirect/linux.c:307
#, fuzzy, c-format
msgid "Could not seek to offset: %x on '%s' (%m)."
msgstr "Ezin izan da '%s' itxi (%m)."

#: usbdiskdirect/linux.c:329
#, fuzzy, c-format
msgid "Could not write to '%s' (%m)."
msgstr "Ezin izan da atakan idatzi (%m)"

#: usbdiskdirect/linux.c:350
#, fuzzy, c-format
msgid "Could not read from '%s' (%m)."
msgstr "Ezin izan da '%s' itxi (%m)."

#: usbscsi/linux.c:92
#, fuzzy, c-format
msgid "Device '%s' is locked by another app."
msgstr "'%s' gailua %d pid-ek blokeatuta dauka"

#: usbscsi/linux.c:97
#, fuzzy, c-format
msgid "Failed to lock '%s' (%m)."
msgstr "Huts egin du '%s' irekitzean (%m)."

#: usbscsi/linux.c:113
#, fuzzy, c-format
msgid "Failed to unlock '%s' (%m)."
msgstr "Huts egin du '%s' irekitzean (%m)."

#: usbscsi/linux.c:214
msgid "USB Mass Storage raw SCSI"
msgstr ""

#: usbscsi/linux.c:325
#, fuzzy, c-format
msgid "Could not send scsi command to: '%s' (%m)."
msgstr "Ezin izan da %d/%d aukerazko konfigurazioa ezarri (%m)"

#, fuzzy, c-format
#~ msgid "Called for filename '%s'."
#~ msgstr "Huts egin du '%s' irekitzean (%m)."

#, fuzzy, c-format
#~ msgid "Could not load '%s': '%s'."
#~ msgstr "Ezin izan da '%s' itxi (%m)."

#, fuzzy, c-format
#~ msgid "Could not find some functions in '%s': '%s'."
#~ msgstr "Ezin izan da '%s' hustu (%m)."

#, fuzzy, c-format
#~ msgid "Could not load port driver list: '%s'."
#~ msgstr "Ezin izan da '%s' gailua blokeatu"

#, fuzzy, c-format
#~ msgid "Could not load '%s' ('%s')."
#~ msgstr "Ezin izan da '%s' itxi (%m)."

#, fuzzy, c-format
#~ msgid "Could only write %i out of %i byte(s)"
#~ msgstr "Ezin izan da atakan idatzi (%m)"

#, fuzzy, c-format
#~ msgid "Getting level of pin %i..."
#~ msgstr "Ezin izan da %i pin-aren maila lortu (%m)."

#, fuzzy
#~ msgid "Clear halt..."
#~ msgstr "clear_halt"

#~ msgid "msg_read"
#~ msgstr "msg_read"

#, c-format
#~ msgid "Device has driver '%s' attached, detaching it now."
#~ msgstr "Gailuak '%s' kontrolatzailea erantsita du, askatu orain."

#, c-format
#~ msgid ""
#~ "USB device (vendor 0x%x, product 0x%x) is a mass storage device, and "
#~ "might not function with gphoto2. Reference: %s"
#~ msgstr ""
#~ "USB gailua (0x%x hornitzailea, 0x%x produktua) biltegiratze-gailua da, "
#~ "eta ez du gphoto2-rekin funtzionatuko. Erreferentzia: %s"

#~ msgid "Could not load any io-library because '%s' could not be opened (%m)"
#~ msgstr ""
#~ "Ezin izan da sarrerako/irteerako liburutegia kargatu, '%s' ezin izan "
#~ "delako ireki (%m)"

#~ msgid "open"
#~ msgstr "ireki"

#~ msgid "close"
#~ msgstr "itxi"

#~ msgid "write"
#~ msgstr "idatzi"

#~ msgid "read"
#~ msgstr "irakurri"

#~ msgid "check_int"
#~ msgstr "check_int"

#~ msgid "update"
#~ msgstr "eguneratu"

#~ msgid "get_pin"
#~ msgstr "get_pin"

#~ msgid "set_pin"
#~ msgstr "set_pin"

#~ msgid "send_break"
#~ msgstr "send_break"

#~ msgid "flush"
#~ msgstr "hustu"

#~ msgid "find_device"
#~ msgstr "find_device"

#~ msgid "find_device_by_class"
#~ msgstr "find_device_by_class"

#~ msgid "msg_write"
#~ msgstr "msg_write"

#~ msgid "Camera is supported by USB Storage driver."
#~ msgstr "USB biltegi kontrolatzaileak kamera onartzen du"
